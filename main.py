# Copyright 2012 by Al Sweigart al@inventwithpython.com
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import random, pygame, sys

from pygame.locals import *





pygame.init()
pygame.display.set_caption('Super L❤️ve')





# Initialise the game and start the game loop
def main():
	ui.uiTestHarness()
	print("All tests succeeded!")

	global UNPAUSED, CURRENT_SCREEN
	UNPAUSED = 0
	CURRENT_SCREEN = 'title_screen'

	ui.initIcons()

	displayTitleScreen()
	while True:
		gameLoop()
#main()





# main game loop
def gameLoop():
	global CURRENT_SCREEN

	# get input
	key_pressed = checkForKeyPress()
	mouse_pressed = checkForMousePress()

	# update game
	if CURRENT_SCREEN == "title_screen":
		if key_pressed or mouse_pressed:
			CURRENT_SCREEN = "in_game"	
	elif CURRENT_SCREEN == "in_game":
		if mouse_pressed:
			for element in ui.UI_ELEMENTS:
				if element.collisionBox.collidepoint(pygame.mouse.get_pos()):
					element.onClick()


	# render to screen
	if CURRENT_SCREEN == "title_screen":
		displayTitleScreen()
	elif CURRENT_SCREEN == "in_game":
		displayInGameScreen()
	pygame.display.update()
#gameLoop()





def checkForKeyPress():
	if len(pygame.event.get(QUIT)) > 0:
		terminate()

	keyUpEvents = pygame.event.get(KEYUP)

	if len(keyUpEvents) == 0:
		return None
	else:
		if keyUpEvents[0].key == K_ESCAPE:
			terminate()
		else:	
			return keyUpEvents[0].key
#checkForKeyPress()




def checkForMousePress():
	mouseUpEvents = pygame.event.get(MOUSEBUTTONUP)

	if len(mouseUpEvents) == 0:
		return None
	elif len(mouseUpEvents) > 0:
		return mouseUpEvents[0]
#checkForMousePress()





import ui


def displayTitleScreen():
	ui.drawLoginScreen()
#displayTitleScreen()




def displayInGameScreen():
	ui.drawDesktop()
#displayInGameScreen()





def terminate():
	pygame.quit()
	sys.exit()
#terminate()





if __name__ == '__main__':
	main()
