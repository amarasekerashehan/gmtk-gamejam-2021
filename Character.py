from Globals import *
from Personality import *

from pathlib import Path
import csv
import re

CAHR_TABLE_FILEPATH = Path("tables/Character Traits - Characters.csv")
AVATARS_FOLDER = Path("assets/avatars")
AVATAR_PLACEHOLDER = "impossibledave.png"

# Loads Characters from file
def loadCharsFromFile(filePath=CAHR_TABLE_FILEPATH):
	characters = {}
	with open(filePath) as file:
		reader = csv.reader(file)

		header = next(reader)
		if loadCharsFromFile.DB: print(header)

		# Find which columns contain the data we are after
		iReady 	= header.index("Asset Complete")
		iDevID 	= header.index("Character Dev IDs")
		iName 	= header.index("Name")
		iBio 	= header.index("Bio")
		iGender = header.index("Gender")
		iAvatar = header.index("Avatar")


		iTraits = {}
		for trait in TYPES.keys():
			iTraits[trait] = header.index(trait)

		iQuirks = {}
		for i in range(1, QUIRKS_NUM+1):
			iQuirks[i] = header.index(f"Quirk {i}")


		for row in reader:
			if loadCharsFromFile.DB: print(row)

			if row[iReady] == "TRUE":

				name 	= row[iName]
				bio 	= row[iBio]
				gender 	= row[iGender]
				avatar  = row[iAvatar]

				# Read Traits
				traits = {}
				for trait, i in iTraits.items():
					if row[i] == 'x': continue # Skip if Quirk is missing

					match = re.match(r"\s*(\d+)\s+(\w+)\s*", row[i])
					if not match: raise Exception(f"Invalid trait format: {row[i]}")

					type, typeVal = match[2], int(match[1])
					traitCheck, traitVal = getTraitValue(type, typeVal)

					if trait == traitCheck:
						traits[trait] = traitVal
					else:
						raise Exception(f"Invalid trait type: {row[i]}")

				# Read Quirks
				quirks = []
				for i in iQuirks.values():
					quirkDevID = row[i] if row[i] != 'x' else None

					# TODO: initiate quirk instances here
					quirks.append(quirkDevID) 

				global M, W, R
				personality = Personality(traits[M], traits[W], traits[R])
				# TODO: Test this
				# for quirk in quirks:
				# 	personality.addQuirk(quirk)

				characters[name] = Character(name, bio, gender, personality, avatar)

		return characters

loadCharsFromFile.DB = False # Toggle debugging for the function above




class Character:
	def __init__(self, name, bio=None, gender=None, personality=None, avatar=AVATAR_PLACEHOLDER):
		self.name = name
		self.bio = bio
		self.gender = gender

		self.avatar = AVATARS_FOLDER / avatar

		self.personality = personality if personality else Personality()

	def __repr__(self):
		string = f"CHAR: {self.name}"

		attrs = ''
		if self.bio: 	attrs += 'B' 
		if self.gender: attrs += 'G'

		if attrs: string += f" ({attrs})"

		return string


	def __str__(self):
		string = f"CHAR: {self.name}\n"
		for name, var in vars(self).items(): # Gets all fields of this instance
			string += f"{name}: {repr(var)}\n"
		return string








# Only runs if this file is runs separately (for debugging purposes)
if __name__ == "__main__":
	chars = loadCharsFromFile()
	for char in chars.values():
		print(str(char))

