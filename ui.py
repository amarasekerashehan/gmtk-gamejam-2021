import pygame, Globals
import ContactsApp



BG_COLOR   = Globals.PURPLE

displayInfo = pygame.display.Info()
WINDOW_HEIGHT = displayInfo.current_h
WINDOW_WIDTH  = displayInfo.current_w
DISPLAY_SURF = Globals.DISPLAY_SURF

THUMBNAIL_HEIGHT = 64


BASIC_FONT = pygame.font.SysFont('Arial', 18)


UI_ELEMENTS = []
DESKTOP_ICONS = []
OPEN_WINDOW = None





class Icon:
	# string representing icon name
	name_string = None
	# object representing name below thumbnail
	name = None
	thumbnail = None
	# part that can be clicked on
	collisionBox = None
	#x-y coordinates of those elements
	namePos = (0, 0)
	thumbnailPos = (0, 0)
	collisionBoxPos = (0, 0)
	
	currentlySelected = False

	def __init__(self, name_string, thumbnail):
		self.name_string = name_string
		self.thumbnail = thumbnail
		UI_ELEMENTS.append(self)
	#__init__()
 
	def __str__(self):
		string = "Icon:\n"
		string += f"Name: {self.name_string}\n"
		return string
	#__str__()

	def onClick(self):
		global DESKTOP_ICONS
		global OPEN_WINDOW
		if self.currentlySelected == False:
			# deselect all other icons
			for icon in DESKTOP_ICONS:
				icon.currentlySelected = False
			# select this icon	
			self.currentlySelected = True
		else:
			OPEN_WINDOW = Window(self.name_string)
	#onClick()
#class Icon





class Window:
	name = None
	contentsPos = (0, 0)
	contentsWidth = 0
	contentsHeight = 0

	def __init__(self, name):
		self.name = name
	#__init__()

#class Window





def initIcons():
	# load in thumbnail images
	contacts_thumbnail  = pygame.image.load('assets/contacts.png')
	superlove_thumbnail = pygame.image.load('assets/superlove.png')
	chatter_thumbnail   = pygame.image.load('assets/chatter.png')

	# assign thumbnails to icons
	contacts_icon  = Icon("Contacts", contacts_thumbnail)
	superlove_icon = Icon("Super Love", superlove_thumbnail)
	chatter_icon   = Icon("Chatter", chatter_thumbnail)

	#put icons in a global array
	global DESKTOP_ICONS 
	DESKTOP_ICONS = [contacts_icon, superlove_icon, chatter_icon]

	# scale the icons thumbnails to be the right size
	for icon in DESKTOP_ICONS:
		icon.thumbnail = pygame.transform.scale(icon.thumbnail, (THUMBNAIL_HEIGHT, THUMBNAIL_HEIGHT))

	# determine	icon positions
	heightBetweenIcons = 30
	widthBetweenIcons = 50
	# start from the top
	currentHeight = 0
	# space above the first icon
	currentHeight += heightBetweenIcons
	for i in range(3):
		# init thumbnail
		icon = DESKTOP_ICONS[i]
		# x coord
		icon.thumbnailPos = (widthBetweenIcons, currentHeight)

		thumbnail_width = icon.thumbnail.get_rect().w
		thumbnail_height = icon.thumbnail.get_rect().h

		currentHeight += THUMBNAIL_HEIGHT

		# init icon name below
		icon.name = BASIC_FONT.render(icon.name_string+".exe", 1, Globals.WHITE)
		name_width = icon.name.get_rect().w
		name_height = icon.name.get_rect().h
		# centre name below thumbnail
		icon.namePos = (widthBetweenIcons + thumbnail_width/2 - name_width/2, currentHeight)

		currentHeight += name_height

		# add a collisionBox so that it can be clicked on
		icon.collisionBoxPos = (icon.namePos[0] - 10, icon.thumbnailPos[1] - 10)
		icon.collisionBox = pygame.Rect((icon.collisionBoxPos), (name_width + 20, thumbnail_height+name_height + 20))


		# leave a space
		currentHeight += heightBetweenIcons
#initIcons()




def drawLoginScreen():
	gameLogo = pygame.image.load('assets/super_love_placeholder_logo.png')

	DISPLAY_SURF.fill(BG_COLOR)
	DISPLAY_SURF.blit(gameLogo, (100, 100))
#drawLoginScreen()





def drawWallpaper():
	global DISPLAY_SURF
	DISPLAY_SURF.fill(BG_COLOR)
#drawWallpaper()




def drawTaskbar():
	global TASKBAR_HEIGHT 
	TASKBAR_HEIGHT = 50
	pygame.draw.rect(DISPLAY_SURF, Globals.DARK_GRAY, (0, WINDOW_HEIGHT - TASKBAR_HEIGHT, WINDOW_WIDTH, TASKBAR_HEIGHT))
#drawTaskbar()





# position and draw the icons
def drawIcons():
	for i in range(3):
		icon = DESKTOP_ICONS[i]
		# draw an thumbnail
		DISPLAY_SURF.blit(icon.thumbnail, (icon.thumbnailPos[0], icon.thumbnailPos[1]))
		# draw icon name below
		DISPLAY_SURF.blit(icon.name, (icon.namePos[0], icon.namePos[1]))
#drawIcons





def drawSelectionBox():
	global DISPLAY_SURF
	for i in range(3):
		icon = DESKTOP_ICONS[i]
		if icon.currentlySelected:
			width = icon.collisionBox.w
			height = icon.collisionBox.h
			selectionBox = pygame.Surface((width, height))
			selectionBox.set_alpha(65)
			selectionBox.fill(Globals.BLUE)
			DISPLAY_SURF.blit(selectionBox, icon.collisionBoxPos)
#drawSelectionBox()





def drawAppWindow(openWindow):
	global DISPLAY_SURF, TASKBAR_HEIGHT

	spaceOnLeft = 175
	titleBarHeight = 50
	openWindow.contentsPos = (175, 50)
	openWindow.contentsHeight = WINDOW_HEIGHT - titleBarHeight - TASKBAR_HEIGHT
	openWindow.contentsWidth = WINDOW_WIDTH - spaceOnLeft

	# title bar (window name, close button, etc.)
	titleBar = pygame.Surface((WINDOW_WIDTH - spaceOnLeft, titleBarHeight))
	titleBar.fill(Globals.BLACK)
	DISPLAY_SURF.blit(titleBar, (spaceOnLeft, 0))

	# window background
	background = pygame.Surface((openWindow.contentsWidth, openWindow.contentsHeight))
	background.fill(Globals.INDIGO)
	DISPLAY_SURF.blit(background, (spaceOnLeft, titleBarHeight))

	# draw window contents
	drawWindowContents(openWindow)
#drawAppWindow()





def drawWindowContents(openWindow):
	if openWindow.name == "Contacts":
		ContactsApp.drawWindowContents(openWindow)
	#other files go here
	
#drawWindowContents()





def drawDesktop():
	global OPEN_WINDOW
	drawWallpaper()
	drawTaskbar()
	drawIcons()
	drawSelectionBox()
	if OPEN_WINDOW:
		drawAppWindow(OPEN_WINDOW)
#drawDesktop()





def uiTestHarness():
	return None
#uiTestHarness()