import pygame, Globals





DISPLAY_SURF = Globals.DISPLAY_SURF





# currently overrites the entire window contents in red. Probably want other UI elements instead
def drawWindowContents(openWindow):
	global DISPLAY_SURF
	windowContents = pygame.Surface((openWindow.contentsWidth, openWindow.contentsHeight))
	windowContents.fill(Globals.RED) 
	DISPLAY_SURF.blit(windowContents, openWindow.contentsPos)
#drawWindowContents()