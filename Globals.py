import pygame, random





# UI

DISPLAY_SURF = pygame.display.set_mode((0,0),pygame.FULLSCREEN)
#             R    G    B
WHITE      = (255, 255, 255)
BLACK      = (  0,   0,   0)
BLUE		= (0,	0,	255)
RED        = (255,   0,   0)
GREEN      = (  0, 255,   0)
DARK_GREEN = (  0, 155,   0)
DARK_GRAY  = ( 40,  40,  40)
PURPLE     = (128,   0, 128)
INDIGO     = ( 75,   0, 130)





# Traits
M = "Morality"
W = "Weirdness"
R = "Romance"
ERR = "UNDEFINED"

TYPES = {
	M: ["Good", "Villain"],
	W: ["Weird", "Normal"],
	R: ["Romantic", "Hedonist"]
}

THRESHOLD = 10 # Threshold for deciding trait type

QUIRKS_NUM = 6 # Number of Quirks per Character 

# Find Type given Trait value i.e. Morality 1 -> Evil 
def getTypeFromTrait(trait, value):
	return TYPES[trait][0] if value > THRESHOLD else TYPES[trait][1]

# Find Trait given Type i.e. Evil -> Morality
def getTraitFromType(type):
	for trait, types in TYPES.items():
		if type in types: return trait
	return ERR

# Convert from type value to trait value i.e. Evil 10 -> Morality 1
def getTraitValue(type, value):
	# print("type", type)
	trait = getTraitFromType(type)
	idx = TYPES[trait].index(type)

	if idx == 0:return (trait, THRESHOLD + value) 
	else:		return (trait, THRESHOLD - (value - 1))

# Convert from trait value to type value i.e. Morality 1 -> Evil 10
def getTypeValue(trait, value):
	type = getTypeFromTrait(trait, value)
	idx = TYPES[trait].index(type)

	if idx == 0:return (type, value - THRESHOLD)
	else:		return (type, THRESHOLD - (value - 1))

# Utility Functions
def doRoll(D=20):
	return random.randrange(0,20) + 1

def percentage(num, max):
	return (num / max) * 100 

# Only runs if this file is runs separately (for debugging purposes)
if __name__ == "__main__":
	# Test getTraitValue 
	# for types in TYPES.values():
	# 	for type_ in types:
	# 		print(getTraitFromType(type_))

	# Test getTraitValue
	# for types in TYPES.values():
	# 	for type_ in types:
	# 		value = random.randrange(0,10) + 1
	# 		print(type_, value, end=' -> ')
	# 		trait, absValue = getTraitValue(type_, value)
	# 		print(trait, absValue)
		
	for trait in TYPES.keys():
		value = random.randrange(0,10) + 1
		print(trait, value, end=' -> ')
		type, absValue = getTypeValue(trait, value)
		print(type, absValue)

		value = random.randrange(10,20) + 1
		print(trait, value, end=' -> ')
		type, absValue = getTypeValue(trait, value)
		print(type, absValue)
