from Globals import *
import Quirks

import random
import math


class Personality:
	minT = 0
	maxT = 20
	traitNum = 3
	maxDiff = math.sqrt(((maxT - minT) ** 2) * traitNum)

	def __init__(self, M=None, W=None, R=None):
		self.traits = {}
		self.appearance = {}
		self.persuasion = {}

		self.quirks = []

		self['Morality']  = M if M else random.randrange(0,20) + 1
		self['Weirdness'] = W if W else random.randrange(0,20) + 1
		self['Romance']   = R if R else random.randrange(0,20) + 1

	def __repr__(self):
		return f"Personality [M:{self[M]} W:{self[W]} R:{self[R]}]"


	def __str__(self):
		string = "Personality:\n"
		string += f"Morality:{self[M]}\n"
		string += f"Weirdness:{self[W]}\n"
		string += f"Romance:{self[R]}\n"
		return string


	def __getitem__(self, key):
		return self.traits[key]
  
	def __setitem__(self, key, newvalue):
		self.traits[key] = newvalue


	def addQuirk(self, quirk):
		if not issubclass(type(quirk), Quirks.Quirk):
			raise Exception("Must be a Quirk or a list of Quirks")

		self.quirks.append(quirk)





# Only runs if this file is runs separately (for debugging purposes)
if __name__ == "__main__":
	P1 = Personality(M=18, W=2, R=17)
	P2 = Personality(M=1, W=16, R=17)

	print(P1)
	print(P2)

	Q = Quirks.exAssassin()

	Q.apply(P1, P2)







