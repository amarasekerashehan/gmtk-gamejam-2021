from Globals import *

import random

# Abstract Base class (only for sub-typing)
class Quirk:
	DB = True # Turn Debugging on/off for this class
	def __init__(self):
		self.trait = ERR

	def execute(self, agent, target):
		difficulty = abs(agent[self.trait] - target[self.trait])

		roll = doRoll()
		success = difficulty <= roll

		diff = abs(difficulty - roll) 

		if   diff <= 5:		score = 5
		elif diff <= 10:	score = 15
		else: 				score = 20

		score = score if success else -score

		if self.DB:
			print(f"|{difficulty} - {roll}| = {diff}")
			print("SUCCESS" if success else "FAILURE")
			# print(f"SCORE = {score}")

		return score
		


class exAssassin(Quirk):
	def __init__(self):
		global M
		self.trait = M
		self.type = TYPES[M][1]

# Only runs if this file is runs separately (for debugging purposes)
if __name__ == "__main__":
	P1 = Personality()
	P2 = Personality()

	print(P1)
	print(getTraitType(M, P1["Morality"]))
